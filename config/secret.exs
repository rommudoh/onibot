use Mix.Config;

# sample secret config options
config :onibot,
  base_url: "https://example.com",
  client_id: "123456",
  client_secret: "123456",
  access_token: "123456"
