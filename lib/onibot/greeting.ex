defmodule Onibot.Greeting do
  use Agent

  @doc """
  Starts a new Greeting
  """
  def start_link(opts) do
    IO.write("Hello, #{opts}!\n")
    Agent.start_link(&initial_state/0)
  end

  # Defines the initial state
  defp initial_state, do: %{}
end
