defmodule Onibot.MastodonConnection do
  use GenServer

  @bearer_token Application.get_env(:onibot, :access_token)
  @base_url Application.get_env(:onibot, :base_url)

  ### GenServer API

  @doc """
  GenServer.init/1 callback
  """
  def init(state), do: {:ok, state}

  @doc """
  GenServer.handle_call/3 callback
  """
  def handle_call({:post, text}, _from, state) do
    try do
      result = Hunter.Status.create_status(state, text)
      {:reply, result, state}
    catch
      _ -> {:reply, :error, state}
    end
  end

  @doc """
  GenServer.handle_cast/2 callback
  """
  def handle_cast(:connect, _state) do
    conn = Hunter.new([
      base_url: @base_url,
      bearer_token: @bearer_token
    ])
    {:noreply, conn}
  end

  ### Client API / Helper functions

  def start_link(state \\ %{}) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  def connect, do: GenServer.cast(__MODULE__, :connect)
  def post(text), do: GenServer.call(__MODULE__, {:post, text})
end
