defmodule Onibot do
  @moduledoc """
  Documentation for Onibot.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Onibot.hello()
      :world

  """
  def hello do
    :world
  end
end
