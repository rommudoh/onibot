# Onibot

This is a Social Networking Bot for servers providing the Mastodon API.

It uses [hunter](https://hexdocs.pm/hunter) for handling the connection.

## Usage

To build it, first fetch the dependencies:

```sh-session
$ mix deps.get
```

If you want to run the automated tests for this project:

```sh-session
$ mix test
```

To run the bot, add your access token to the config and run:

```sh-session
$ mix run --no-halt
```

